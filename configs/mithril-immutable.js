var core = require('../configs/core')
var hyperscript = require('../modules/hyperscript')

module.exports = hyperscript({
    tagName: ['tag']
    ,attrs: ['attrs']
    ,children: ['children']
    ,props: ['attrs']
    ,events: ['attrs']
    ,style: ['attrs', 'style']
}, core )