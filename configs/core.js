
const { path, assocPath } = require('../utils/path-immutable')
const core = require('../modules/core')

module.exports = core(assocPath, path)