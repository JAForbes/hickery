module.exports = function(
    prefixes
    ,$
){

    $.root(
        o => {
            o.className= ['attrs','className']
            o.children = ['children']
        }
    ) ( prefixes )

    const prefixed = 
        Object.keys(prefixes)
            .reduce( 
                (p,k) => {
                    p[k] = $.path( prefixes[k] )
                    return p
                }
                ,{}
            )

    const out = Object.assign({
        classList: {
            over: f => (s='') => 
                f( s.split(' ') )
                .filter(Boolean)
                .join(' ')

            ,add: className => out.classList.over(
                xs => xs.concat(className)
            )

            ,remove: className => out.classList.over(
                xs => xs.filter( x => x && x != className )
            )
        }
        ,child: {
            nth: n => f => prefixed.children ( $.nth (n) (f) )
            ,first: f => prefixed.children ( $.nth(0) (f) )
            ,last: f => prefixed.children( $.last(f) )
            ,all: f => prefixed.children( $.all(f) )
        }
        
    }, $, prefixed)

    return out
}