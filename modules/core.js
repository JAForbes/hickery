function Core(_assocPath, _path){
    
    // eslint-disable-next-line fp/no-rest-parameters
    const compose = (...fns) => (...xs) =>
        fns.reduceRight( (p,n) => [n(...p)], xs)[0]

    const merge = a => b => Object.assign({}, a, b)

    const $root = f => x => {
        const r = f(x)
        if( typeof r !== 'undefined'){
            return r
        } else {
            return x
        }
    }

    const PureQueryOr = 
        otherwise =>
        getter =>
        setter =>
        f => 
        o => {
          
        var vMaybe = getter(o)
        
        var v =
            typeof vMaybe != 'undefined'
            ? vMaybe
            : otherwise

        return setter ( f(v) ) (o)
    }
    
    const QueryOr = 
        otherwise => getter => setter => f => $root(
            PureQueryOr (otherwise) ( getter ) (setter) (f)
        )

    const path = k => o => _path(k, o)
    const assocPath = k => value => o =>
        _assocPath(k, value, o)

    const PureQuery = PureQueryOr ( undefined )

    const Query = QueryOr(undefined)
   
    const $pathOr = otherwise => k =>
        PureQueryOr ( otherwise ) ( path(k) ) ( assocPath(k) ) 

    const $path =
        $pathOr (undefined)

    const $nth = n => f => $root(
        xs => assocPath ([n]) (f(xs[n])) (xs)
    )

    const $last = f => $root(
        xs => xs.length > 0 
            ? $nth(xs.length - 1) (f) (xs)
            : $nth ( 0 ) ( f ) (xs)
    )

    const $all =
        f => xs => xs.map( x => f(x) )

    const select = f => o => {
        const r = []
        f( ( x ) => {
            r.push(x) // eslint-disable-line fp/no-mutating-methods
            return x
        } )(o)
        return r
    }

    const lens = {
        view: $q => o => select ($q) (o) [0]
        ,over: $q => f => $q(f)
        ,set: $q => x => lens.over( $q ) ( () => x )
    }

    const set = x => () => x

    return {
        PureQueryOr
        ,QueryOr
        ,PureQuery
        ,Query
        ,root: $root
        ,pathOr: $pathOr
        ,path: $path
        ,nth: $nth
        ,last: $last
        ,all: $all
        ,lens
        ,set
        ,select
        ,merge
        ,compose
    }
}

module.exports = Core
