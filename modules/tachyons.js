const b = require('bss')

if( typeof hickery != undefined ){
    // eslint-disable-next-line no-undef
    hickery.tachyons = tachyons(hickery)
}

function tachyons($){

    const merge = $.merge

    const CSSProp = k => f => $.style( o => {

        var r = {}
        // eslint-disable-next-line fp/no-mutation
        r[k] = f(o[k])


        return $.merge(r)(o)
    })

    const $bg = CSSProp('backgroundColor')

    const $color = CSSProp('color')

    const dirs =
        ['left', 'right', 'top', 'bottom']

        
    const $keyframe = f => {
        var pValue = {}
        
        return $.style(o => {
            // eslint-disable-next-line fp/no-mutation
            pValue = f(pValue)
        
            return merge(o)({ 
                animationName: b.$keyframes(pValue) 
            })
        })
    }
    
    const $hover = f => {
        var pValue = {}
        var pClass;
        
        return o => {
            const remove =
                typeof pClass != 'undefined'
                ? $.class.remove(pClass)
                : i => i
            
            // eslint-disable-next-line fp/no-mutation
            pValue = f(pValue)
            
            // eslint-disable-next-line fp/no-mutation
            pClass = b.$hover(pValue).class
            
            const add = $.class.add(pClass)
            
            return $.className(
                $.compose(
                    add
                    ,remove
                )
            )(o)
        }
    }
  
    const $media = f => {
        var pValue = {}
        var pClass;

        return o => {
            const remove =
                typeof pClass != 'undefined'
                ? $.class.remove(pClass)
                : i => i
            
            // eslint-disable-next-line fp/no-mutation
            pValue = f(pValue)
            
            // eslint-disable-next-line fp/no-mutation
            pClass = b.$media(b(pValue)).class
            
            const add = $.class.add(pClass)
            
            return $.className(
                $.compose(
                    add
                    ,remove
                )
            )(o)
        }
        // $media('(max-width: 600px)', b.color('blue'))
    }

    const x = prefix => dirs => n =>
        $.style(
            merge(
                dirs
                    .map( k => prefix+'-'+k )
                    .map( k => ({ [k]: n }) )
                    .reduce( (p,n) => merge(n)(p), {})
            )
        )
    const $absolute = CSSProp('absolute')
    const $relative = CSSProp('relative')
    const $top = CSSProp('top')
    const $right = CSSProp('right')
    const $left = CSSProp('left')
    const $bottom = CSSProp('bottom')

    const p = x('padding')
    const ph = p(dirs.slice(0,2))
    const pv = p(dirs.slice(2))
    const pa = p(dirs)
    const pl = p([dirs[0]])
    const pr = p([dirs[1]])
    const pt = p([dirs[2]])
    const pb = p([dirs[3]])

    const m = x('margin')
    const mh = m(dirs.slice(0,2))
    const mv = m(dirs.slice(2))
    const ma = m(dirs)
    const ml = m([dirs[0]])
    const mr = m([dirs[1]])
    const mt = m([dirs[2]])
    const mb = m([dirs[3]])

    const $fontFamily = CSSProp('fontFamily')
    const $display = CSSProp('display')
    const dib = $display($.set('inline-block'))
    const db = $display($.set({ display: 'block' }))
    const helvetica = $fontFamily($.set({ fontFamily: 'helvetica' }))
    const $borderRadius = CSSProp('borderRadius')
    const $textAlign = CSSProp('textAlign')
    const tc = $textAlign( $.set('center') )
    
    return {
        pa, ph, pv, pt, pb, pl, pr
        ,ma, mh, mv, mt, mb, ml, mr
        ,$bg
        ,$color
        ,$absolute
        ,$relative
        ,$top
        ,$left
        ,$right
        ,$bottom
        ,CSSProp
        ,$fontFamily
        ,$display
        ,$textAlign
        ,dib
        ,db
        ,$hover
        ,$media
        ,$keyframe
        ,helvetica
        ,$borderRadius
        ,$br: $borderRadius
        ,tc
    }
}

module.exports = tachyons