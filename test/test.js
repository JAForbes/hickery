/* eslint-disable fp/no-mutation, fp/no-delete */
const R = require('ramda')

const core = require('../configs/core')
const hyperscript = require('../modules/hyperscript')

const h = (tag, attrs, children) => ({ tag, attrs, children })
const test = require('tape')



const $ = hyperscript({
    tagName: ['tag']
    ,attrs: ['attrs']
    ,props: ['attrs']
    ,children: ['children']
    ,style: ['attrs', 'style']
    ,events: ['on']
}, core )

test('The basics', function(t){

    const tree = () => h('div', {}, [])
    
    {
        const out = $.root(R.merge({ a:1 }))({ b: 2 })

        t.deepEquals(
            out
            ,{ a:1, b:2 }
            ,'Root can modify the source'
        )
    }

    {
        const query =
            $.children(function(xs){
                return xs.concat( h('h1', {}, ['hello']) )
            })

        t.deepEquals(
            query(tree()).children[0].children
            ,['hello']
            , 'Children were modified within the query'
        )
    }

    {
        t.deepEquals(
            $.merge({ a:1 })({ b:2 })
            ,{ a:1, b:2 }
            ,'Merge'
        )
    }

    {
        const query =
            $.tagName( s => s.toUpperCase() )

        t.equals(
            query( tree() ).tag
            ,'DIV'
            , 'Tag was uppercased within query'
        )
    }

    {
        const query =
            $.props( o => Object.assign( o, { disabled: true } ))

        t.equals(
            query( tree() ).attrs.disabled
            ,true
            , 'Props were updated'
        )
    }

    {
        const query =
            $.attrs( () => ({ cool: 'yes' }) )

        t.equals(
            query( tree() ).attrs.cool
            ,'yes'
            , 'Attrs were replaced'
        )
    }

    {
        const query =
            $.events( () => ({ click: () => {} }) )

        t.equals(
            typeof query( tree() ).on.click
            ,'function'
            , 'Events writes to on.click (snabbdom style)'
        )
    }

    {
        const noChildren = tree()
            noChildren.children = []

        const lastQuery =
            $.child.last( c => c * 10 )
        
        t.equals(
            lastQuery(tree()).children.length
            ,1
            ,`Child queries inject item if their index doesn't exist`
        )

    }

    {
        const root =
            [ tree()
            , $.children( R.concat([ h('p',{},['First child']) ]) )
            , $.child.nth(0) ( () => 100 )
            , $.child.last( c => c * 10 )
            , $.select( $.child.nth(0) )
            ]
            .reduce( (p,n) => n(p) )

        t.deepEquals(
            root
            ,[1000]
            ,'First child replaced when it exists'
        )


    }

    t.end()
})

test('composition', function(t){
    const $li = $.compose(
        $.child.first
        , $.child.all
    )

    const tree = () =>
        h('div', {}, [
            h('ul', {}, [
                h('li', { className: 'blue' }, '1')
                ,h('li', { className: 'green blue' }, '2')
                ,h('li', { disabled: true }, '3')
            ])
        ])


    const action$updateTree =
        $li(
            $.compose(
                $.className(
                    $.compose(
                        $.classList.add('red'),
                        $.classList.remove('green')
                    )
                )
                ,$.attrs(
                    R.omit(['disabled'])
                )
            )
        )

    const original = tree()
    const after = action$updateTree( tree() )

    const viewClassNames =
        $.select($.compose($li, $.className))

    const viewDisabled =
        $.select( $.compose($li, $.attrs, $.path(['disabled']) ) )

    t.deepEquals(
        viewDisabled( original ).concat(viewDisabled(after)),
        [undefined, undefined, true, undefined, undefined, undefined]
        ,`Disabled was removed from all li's`
    )

    t.deepEquals(
        viewClassNames( original ).concat(viewClassNames(after)),
        [ 'blue', 'green blue', undefined, 'blue red', 'blue red', 'red' ]
        ,`Green was removed and red was added, blue remains`
    )

    t.end()
})

test('view, set and over ', function(t){

    const $disabled =
        $.compose( $.path( [ 'disabled' ]) )

    t.equals(
        $.lens.view($disabled)({ disabled: 'whatever'})
        ,'whatever'
        ,'View... views'
    )

    t.equals(
        $.lens.view ($disabled) ( $.lens.set ($disabled) (true) ({}) )
        ,true
        ,'Set... sets'
    )

    t.deepEquals(
        $.lens.over($disabled)( x => !x )({})
        ,{ disabled: true }
        ,'Over maps over a lens'
    )


    t.end()
})

test('Define custom mutable queries', function(t){
    const $disabled =
        $.Query( R.prop('disabled')) ( v => o => {
            o.disabled = v
        } )

    const original = {}

    $disabled($.set(true))(original)

    t.equals(
        original.disabled
        ,true
        ,'original was mutated'
    )
    t.end()
})

test('Superstylin', function(t){
    const $color = $.compose(
        $.style
        ,$.path(['color'])
    )

    const action$red = $color($.set('red'))

    const dataNull = h('h1', null, 'Split Enz')
    const dataExistingStyle =
        h('h1', { style: { fontSize: '2em' } }, 'Split Enz')
    // I see red I see red I see red
    t.deepEquals(
        $.select ($color) ( action$red (dataNull) )
        ,['red']
        , 'Set styles via composeable actions (null attrs)'
    )

    t.deepEquals(
        $.select ($color) ( action$red (dataExistingStyle) )
        ,['red']
        , 'Set styles via composeable actions (non null attrs)'
    )

    t.end()
})