
const $ = require('../configs/core')

const test = require('tape')

test('The basics', function(t){
    
    const a = { b: { c: {}, d: [1,2,3] } }

    t.deepEquals(
        $.merge({ a: 1, b: 1 }) ({ b: 2, c: 2 })
        ,{ a: 1, b: 2, c: 2 }
        ,'merge'
    )

    t.deepEquals(
        $.Query
            ( x => x.a )
            ( v => x => { 
                x.a = v // eslint-disable-line fp/no-mutation
            } ) 
            ( x => x * x )
            ({ a: 2 })
        ,{ a: 4 }
        ,'Mutable Query'
    )

    t.deepEquals(
        $.root( () => 2 ) ( 2 )
        ,2
        ,'$.root'
    )

    t.deepEquals(
        $.last( $.set('cool') ) ([1,2,3])
        ,[1,2,'cool']
        ,'$.set'
    )

    t.deepEquals(
        $.nth(0) ( x => x * 4 ) (a.b.d)
        ,[4,2,3]
        ,'nth(0)'
    )
    
    t.deepEquals(
        $.compose( x => x[1] + x[0], x => x.split('') ) ( 'hi' )
        ,'ih'
        ,'$.compose'
    )

    t.deepEquals(
        $.all( x => x * 3 )( a.b.d )
        ,[3,6,9]
        ,'$.all'
    )

    t.deepEquals(
        $.select( $.nth(2) ) (a.b.d) //=> [3]
        ,[3]
        ,'$.select( $.nth( len -1 ) )'
    )

    t.deepEquals(
        $.select( $.last ) ([1,2,3,4])
        ,[4]
        ,'select last'
    )

    t.deepEquals(
        $.last($.set('yo')) ([])
        ,['yo']
        ,'create last'
    )

    t.deepEquals(
        $.select( 
            $.compose(
                $.path(['b'])
                , $.path(['d'])
                , $.last 
            )
        ) (a) //=> [3]
        , [3]
        ,'select + compose paths + last'
    )

    t.deepEquals(
        $.select( 
            $.pathOr ('otherwise') (['not','existent'])
        ) (a) //=> ['otherwise']
        ,['otherwise']
        ,'pathOr'
    )

    t.deepEquals(
        
        $.root( (o) => {
            // eslint-disable-next-line fp/no-mutation
            o.a = o.a + '!' 
        } ) ({ a: 'hello' })
        ,{ a: 'hello!' }
        ,'Support mutation $root'
    )

    t.deepEquals(
        $.lens.view ( $.nth(3) ) ( [1,2,3,4] )
        ,4
        ,'$.lens.view'
    )

    t.deepEquals(
        $.lens.set ( $.nth(3) ) (5) ( [1,2,3,4] )
        ,[1,2,3,5]
        ,'$.lens.set'
    )

    t.deepEquals(
        $.lens.over ( $.nth(3) ) ( x => x * x ) ( [1,2,3,4] )
        ,[1,2,3,16]
        ,'$.lens.over'
    )

    t.end()

})