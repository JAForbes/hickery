// istanbul ignore next

function assocPath(path, val, obj) {
  if (path.length === 0) {
    return val;
  }
  var idx = path[0];
  if (path.length > 1) {
    var nextObj = 
        obj != null
            && Object.prototype.hasOwnProperty.call(obj, idx)
            ? obj[idx] 
        : path[1] << 0 == path[1]
            ? [] 
            : {};
    // eslint-disable-next-line no-param-reassign
    val = assocPath(Array.prototype.slice.call(path, 1), val, nextObj);
  }
  if (idx << 0 === idx && Array.isArray(obj)) {
    var arr = [].concat(obj);
    arr[idx] = val;
    return arr;
  } else {
    let result = {}; // eslint-disable-line fp/no-let
    for (let p in obj) { // eslint-disable-line fp/no-let
      result[p] = obj[p];
    }
    result[idx] = val;
    return result;
  }
}

function path(paths, obj) {
  var val = obj;
  var idx = 0;
  while (idx < paths.length) {
    if (val == null) {
      return undefined;
    }
    val = val[paths[idx]];
    idx = idx + 1;
  }
  return val;
}

module.exports = {
    path
    ,assocPath
}