
#### FAQ

### Isn't it slow to decorate VTree's?  How does that scale across an application?

Hickery allows you to opt in to mutability, and for querying virtual dom, you absolutely should.

Because Virtual DOM always returns a new VTree every render, we can safely mutate the VTree before it's handed to the DOM diff algorithm.  Decoration is at the very edge of the system so there is no point in paying the performance cost of immutability.  It will benefit no-one, because the application has already handed that data over to a system that will perform IO.

Hickery is so fast because it's built in queries mutate the VNode directly.  This prevents thrashing the garbage collector and wasting cycles on immutability at a point in the application life cycle where immutability would have no benefit.

If you are reusing VNode's across redraw's you are probably not following your framework of choice's best practices.

Note, even if you've opted into mutability, you can define your own immutable queries via: `$.Query` and `$.QueryOr`.  It's advisable to keep queries for other parts of your application immutable by default.


#### Why does Hickery focus on VNode's in particular?  It seems like a general purpose concept.

I think part of the problem with selling lenses, is that they are applicable everywhere so demonstrations try too hard to prove that generality.  Examples end up being general purpose and that isn't helpful.  You need to start *somewhere* and I think with a few small tweaks, Lenses can solve the component problem.  It seems like a nice juicy problem to solve, and along the way if you write queries to do other things all the better!

Hopefully Hickery fulfills that mission.  If not, hopefully it inspires others to!

TODO: I need to write the component problem post.

#### How do Hickery Queries differ from CSS Sauron libs like mithril-query

Aside from being half the size, Hickery is a more general purpose tool.

It can be used in a very similar fashion to mithril-query  (writing tests).  But Hickery is designed to also be used within application logic.

The implementation and API reflects that.

In a library like mithril-query, view is the default and only option.

```js
// $ = context.find
$('div > ul > li')
```

Hickery's API optimizes for updating the structure and returning the root node.  But you can also target and return the results of a particular query via the `$.select` query.


```js
$div(
    $ul(
        $li( $.select ) // we'll collect all the li's when the query runs
    )
) // and return the complete list
```

If you're using mithril-query, and you wanted to update that list and return the complete tree, you'd need to
reach for another library (shout out to [patchinko](https://github.com/barneycarroll/patchinko) ).  Or do some mutations in a loop in a non declarative way.

Hickery lets you write a query once and then reuse it for both usecases, querying and modification.

#### How do Hickery Queries differ from Lenses

If you don't know what lenses are, or you've struggled with internalizing how they work - this library is a great tool to gain an intuition of why lenses are so valuable.

##### Technical Difference

The technical difference is that Queries do not map over a Functor to transform a context, nor do queries return a Functor. Queries return the raw transformed context (or a value of the same type as the input context).

So if a lens factory is written as such:

```js
(get, set) => f => o
    map( v => set(v, o), f(get(o)))

```

Pure queries are just:

```js
(get, set) => f => a =>
    // Providing f returns the same type as a
    set( f(get(a), a) )
```

Skipping the Functor step is a trade off, it means we are making assumptions about what people use Queries for.  A Lens can inject different Functors to get different behaviors.  Typical use cases are using the `Const` and `Identity` functors to produce `over` and `view`.  But technically you could inject an unbounded variety of Functors and access interesting behaviours without sacrificing composition.

But Queries assume by default we are updating the structure.  When we want to opt out of that, we just pass in `identity`, so that we are effectively updating the value to be what it already is.  In reality, for nearly all use cases we get the exact same functionality but with a simpler interface.

In a language like Haskell, not using a Functor requires justification, using `map` ensures interop with a range of datatypes - for free!  And that's really cool, but most examples of lenses you see do not operate on Functors at all, they are advertised as a way to traverse deeply nested objects.

In Javascript, the Functors we do have are ... messy and awkward.  That's something the Fantasy Land spec seeks to resolve but despite those efforts: Functors aren't an obvious choice in JS.

Hickery queries skip the indirection by optimizing for the common case.  And in doing so we have a simpler interface that doesn't require `view`, `set` and `over` for convenient interaction.

Note that, we don't lose perhaps the most important feature of Twan van Laarhoven lenses: Queries compose using vanilla function composition.

##### Practical Difference

Queries, as defined in this library, as a sibling or cousin of Lenses.  It's lens behavior with a convenient API that encourages DIY composition instead of using predefined operators.

So why diverge from a standard API?  Because despite the fact that Lenses are fantastic, I've found they are quite hard to teach.  Which is frustrating because they solve a lot of problems we encounter in programming all the time.  I've seen other people try to solve this problem by switching from static functions to methods on an context object - but I don't think that gets to the root of the problem.

The problem is that the core abstraction is so hard to get one's head around.  It's a composition of all these other combinators, you need to know what a Functor is, and that a Function can be a Functor, there's a lot of hill to climb before you can see the *view*.

Queries compose just like Van Laarhoven lenses.  But Queries encourage direct application instead of requiring separate functions like `set` and `over`.  Queries emphasise building on top of a pattern familiar to JS developers: `tap` - a function that appears in numerous popular libraries.

You can define a Query that meets all of the Lens laws, and you can trivially define `set` and `over` in terms of Queries (this library currently includes them as `$.lens.set`, `$.lens.over` and `$.lens.view`).

But queries are built for direct interaction:

```js
const someAction = $someQuery(function(someContext){
    // do stuff to context

    // return a new context
})

someAction( someContext )
```

Queries were also designed to support focusing on multiple values simultaneously and virtual values (values that may not even exist).  That's why Hickery's `select` function always returns a list.  It's absolutely possible to do this with Lenses, but aside from partial.lenses, I haven't seen much focus on that path.

Hickery queries are composeable, they're higher order they are a lot like lenses.  But lenses have several constraints that Queries deliberately ignore.  For example, Queries can ignore the return type of a visitor function for side effects.  Lenses always use the return value in a call to `over`.

Lenses also do not mutate the original context.  Queries can opt in to that behaviour where it makes sense.

Queries are an attempt to create a pattern for composing and reusing compositions for accessing nested structures that is light weight, accessible and ... fun!

If you are comfortable with lenses, the only reason I'd recommend Hickery over other lens libraries would be the small payload, convenient interface and UI programming oriented API surface.

If you do want to learn traditional lenses, I recommend checking out one of the following libraries:

- [Partial Lenses](https://www.npmjs.com/package/partial.lenses)
    - 41kb unminified
    - A utility belt library replacement with Lenses as the core abstraction
    - Extremely well maintained and performant.
    - If you have any questions the [calmm-js](https://gitter.im/calmm-js/chat/archives/2017/02/28) gitter room is the best place to ask.

- [Ramda](http://ramdajs.com/)

    - 321kb unminified
    - A general purpose functional programming library
    - Includes several lens utilities
    - If you have any question checkout the [gitter room](https://gitter.im/ramda/ramda)
